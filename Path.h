#pragma once
#include<iostream>
#include<string>
#include"Node.h"

/**
* \brief Robotun gitti�i yollar� tutan s�n�f
*/
class Path 
{
private:
	/**
	* \brief Ba�l� listenin sonu
	*/
	Node *tail;
	/**
	* \brief Ba�l� listenin ba��
	*/
	Node *head;
	/**
	* \brief Ba�l� listedeki eleman say�s�
	*/
	int number;
public:
	/**
	* \brief Ba�l� listeyi olu�turacak yap�c� fonksiyon
	*/
	Path();
	/**
	* \brief Ba�l� listeye eleman eklemesi yapar
	*/
	void addPos(Pose);
	/**
	* \brief Ba�l� listedeki elemanlar� yazd�r�r
	*/
	void print();
	/**
	* \brief Ba�l� listede istenilen indexteki konumu d�nd�r�r
	*/
	Pose operator[](int);
	/**
	* \brief Ba�l� listede istenilen indexteki konumu d�nd�r�r
	*/
	Pose getPos(int);
	/**
	* \brief Ba�l� listede istenilen indexteki konumu sildirir
	*/
	bool removePos(int);
	/**
	* \brief Ba�l� listede istenilen indexe konum atamas� yapar
	*/
	bool insertPos(int, Pose);
	/**
	* \brief Ba�l� listedeki elemanlar� yazd�r�r
	*/
	void operator<<(Path);
	/**
	* \brief Ba�l� listenin sonuna eleman atamas� yapar
	*/
	void operator >> (Pose);
};