#include "RobotOperator.h"
#include "Encryption.h"
#include<iostream>
#include<string>

using namespace std;

Encryption test;

int RobotOperator::encryptCode(int number)
{
	return test.encrypt(number);
}

int RobotOperator::decryptCode(int number)
{
	return test.decrypt(number);
}

RobotOperator::RobotOperator(int number)
{
	name = "Ada";
	surname = "Onsekiz";
	accessCode = encryptCode(number);
	accessState = false;
	
}

bool RobotOperator::checkAcessCode(int number)
{
	if (number == decryptCode(accessCode))
	{
		accessState = true;
		return true;
	}
	else
		return false;
}

void RobotOperator::print()
{
	cout << "The Robot name is: " << name << " " << surname<<endl;
	cout << "Access state is: " << accessState<<endl;
}

