#include "RobotControl.h"
#include"Pose.h"

RobotControl::RobotControl(PioneerRobotAPI *robot)
{
	robotAPI = robot;
	float x, y, th;
	position = new Pose;
	x = robot->getX();
	y = robot->getY();
	th = robot->getTh();
	position->setX(x);
	position->setY(y);
	position->setTH(th);
}

RobotControl::~RobotControl()
{
	
}

void RobotControl::turnLeft()
{
	robotAPI->turnRobot(robotAPI->left);
}

void RobotControl::turnRight()
{
	robotAPI->turnRobot(robotAPI->right);
}

void RobotControl::forward(float speed)
{
	robotAPI->moveRobot(speed);
}

void RobotControl::print(float sonars[],float laserData[])
{
	{
		cout << "MyPose is (" << robotAPI->getX() << "," << robotAPI->getY() << "," << robotAPI->getTh() << ")" << endl;
		cout << "Sonar ranges are [ ";
		robotAPI->getSonarRange(sonars);
		for (int i = 0; i < 16; i++) {
			cout << sonars[i] << " ";
		}
		cout << "]" << endl;
		cout << "Laser ranges are [ ";
		robotAPI->getLaserRange(laserData);
		for (int i = 0; i < 181; i++) {
			cout << laserData[i] << " ";
		}
		cout << "]" << endl;
	}
}

void RobotControl::backward(float speed)
{
	robotAPI->moveRobot(-speed);
}

Pose RobotControl::getPose()
{
	Pose pose;
	pose.setX(position->getX());
	pose.setY(position->getY());
	pose.setTH(position->getTH());

	return pose;
}

void RobotControl::setPose(Pose yeni)
{
	position = &yeni;
}

void RobotControl::stopTurn()
{
	robotAPI->turnRobot(robotAPI->forward);
}

void RobotControl::stopmove()
{
	robotAPI->stopRobot();
}
