#include "Pose.h"
#include<math.h>

Pose::Pose()
{
}
Pose::Pose(float _x)
{
	this->x = _x;
}
Pose::Pose(float _x, float _y)
{
	this->x = _x;
	this->y = _y;
}
Pose::Pose(float _x, float _y, float _th)
{
	this->x = _x;
	this->y = _y;
	this->th = _th;
}
Pose::~Pose()
{
}
float Pose::getX() const
{
	return this->x;
}
void Pose::setX(float x)
{
	this->x = x;
}
float Pose::getY() const
{
	return this->y;
}
void Pose::setY(float y)
{
	this->y = y;
}
float Pose::getTH() const
{
	return this->th;
}
void Pose::setTH(float th)
{
	this->th = th;
}
bool Pose::operator == (const Pose& p)
{
	if ((this->getX() == p.getX()) && (this->getY() == p.getY()) && (this->getTH() == p.getTH()))
		return true;
	else
		return false;
}
Pose Pose::operator + (const Pose& p)
{
	Pose pose;
	pose.setX(this->getX() + p.getX());
	pose.setY(this->getY() + p.getY());
	pose.setTH(this->getTH() + p.getTH());
	return pose;
}
Pose Pose::operator-(const Pose & pos)
{
	Pose pose;
	pose.setX(this->getX() - pos.getX());
	pose.setY(this->getY() - pos.getY());
	pose.setTH(this->getTH() - pos.getTH());
	return pose;
}
Pose & Pose::operator+=(const Pose & pos)
{
	this->setX(this->getX() + pos.getX());
	this->setY(this->getY() + pos.getY());
	this->setTH(this->getTH() + pos.getTH());
	return *this;
}
Pose & Pose::operator-=(const Pose & pos)
{
	this->setX(this->getX() - pos.getX());
	this->setY(this->getY() - pos.getY());
	this->setTH(this->getTH() - pos.getTH());
	return *this;
}
bool Pose::operator<(const Pose & pos)
{
	Pose org(0, 0);
	if (this->findDistanceTo(org) < pos.findDistanceTo(org))
		return true;
	else
		return false;
}
bool Pose::operator>(const Pose & pos)
{
	Pose org(0, 0);
	if (this->findDistanceTo(org) > pos.findDistanceTo(org))
		return true;
	else
		return false;
}
Pose Pose::getPose() const
{
	return *this;
}
void Pose::setPose(float _x, float _y, float _th)
{
	this->x = _x;
	this->y = _y;
	this->th = _th;
}
float Pose::findDistanceTo(Pose pos) const
{
	float x = fabs(pos.getX() - this->getX()) * fabs(pos.getX() - this->getX());
	float y = fabs(pos.getY() - this->getY()) * fabs(pos.getY() - this->getY());
	return pow(x + y, 0.5);
}
float Pose::findAngleTo(Pose pos)
{
	if (*this == pos)
		return 0.0;
	return (atan(fabs((this->getY() - pos.getY())) / fabs(this->getX() - pos.getX()))) * 180 / 3.14;
}