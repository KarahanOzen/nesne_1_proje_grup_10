#pragma once
/*
* \brief Robotun sifresinin olusturuldugu k�s�md�r.
* \author Buket Soyhan
*/
class Encryption {
public:

	/* 
	* \brief 4 basamakl� sifre icin bir sifre girilir ve bu sifre icin gerekli sifrelendirme i�lemleri yap�l�r.
	* \param: data
	*/
	int encrypt(int);
	/*
	* \brief 4 basamakl� sifre icin ��z�mleme yapar.
	* \param: data2
	*/
	int decrypt(int);
};
