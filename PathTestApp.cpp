#include<iostream>
#include"Path.h"
using namespace std;

int main()
{
	Path path;

	Pose pose;
	pose.setX(15);
	pose.setY(20);
	pose.setTH(150);
	path.addPos(pose);
	path.print();

	Pose temppose = path[0];
	cout << temppose.getX() << endl;
	cout << temppose.getY() << endl;
	cout << temppose.getTH() << endl;

	Pose pose2;
	pose2.setX(10);
	pose2.setY(20);
	pose2.setTH(30);
	path.addPos(pose2);

	temppose=path[1];
	cout << temppose.getX() << endl;
	cout << temppose.getY() << endl;
	cout << temppose.getTH() << endl;
	
	Pose pose3;
	pose3.setX(1);
	pose3.setY(2);
	pose3.setTH(3);
	path.addPos(pose3);
	temppose = path[2];
	cout << temppose.getX() << endl;
	cout << temppose.getY() << endl;
	cout << temppose.getTH() << endl;

	if (path.removePos(2))
		cout << "True" << endl;
	else
		cout << "False" << endl;

	Pose pose4;
	pose4.setX(11);
	pose4.setY(22);
	pose4.setTH(33);
	path.addPos(pose4);

	path.insertPos(0, pose4);
	path.print();

	path.removePos(3);

	path << path;

	path >> pose;
	path >> pose2;

	path.print();


	system("Pause");
}