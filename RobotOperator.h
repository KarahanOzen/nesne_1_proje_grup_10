#pragma once
#include"Encryption.h"
#include<iostream>

using namespace std;
/*
* \brief Robotu kumanda edecek operat�r�n yetkilendirilmesinde kullan�lacak s�n�ft�r.
* \author Buket Soyhan
*/
class RobotOperator {
private:
	string name;
	string surname;
	unsigned int accessCode;
	bool accessState;
	int encryptCode(int);
	int decryptCode(int);

public:
	/*
	* \brief �ifrenin do�rulugunu kontrol eder eger dogruysa true dondurur. Yanl�ssa false d�ndurur.
	* \param number: girilen �ifre
	*/
	bool checkAcessCode(int);
	/*
	* \brief Robotun ad�n� soyad�n� ve eri�im durumunu yazd�r�r.
	*/
	void print();
	/*
	* \brief Constructor fonksiyonudur.
	* \param number: girilen  �ifre.
	*/
	RobotOperator(int number);
};

void print();
