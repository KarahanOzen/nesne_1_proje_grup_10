#include"PioneerRobotAPI.h"
#include"RobotControl.h"
#include"RobotOperator.h"
#include"LaserSensor.h"
#include"SonarSensor.h"
#include"Path.h"
#include<iostream>

using namespace std;
/**
* \author Buket Soyhan
*/

PioneerRobotAPI *robot;
float sonars[16];
float laserData[181];

void PowerMenu(RobotOperator);
void MoveMenu(RobotControl);


int main()
{
	robot = new PioneerRobotAPI;
	RobotControl control(robot);
	Path path;

	int choise, accesscode, code;

	cout << "Please create an access code for operator :";
	cin >> accesscode;
	RobotOperator robotOperator(accesscode);

	/**
	* \brief Ana menu
	*/

	while (true)
	{
		cout << "CONTROL CENTER" << endl;
		cout << "1. Power Menu" << endl;
		cout << "2. Move Menu" << endl;
		cout << "0. Exit" << endl << endl;

		cin >> choise;

		if (choise == 1)
		{
			PowerMenu(robotOperator);
		}

		else if (choise == 2)
		{
			MoveMenu(control);
		}

		else if (choise == 0)
		{
			break;
		}
	}
}

/**
* \brief Bagl� olup olmad�g�n� kontrol eden menu
*/

void PowerMenu(RobotOperator robotOperator)
{
	int choise, code;
	while (true)
	{
		cout << "1. On" << endl;
		cout << "2. Off" << endl;
		cout << "0. Back" << endl << endl;

		cin >> choise;

		if (choise == 1)
		{
			cout << "Enter the password: ";
			cin >> code;
			if (robotOperator.checkAccessCode(code))
				robot->connect();
			else
				cout << "Wrong Password!" << endl;
		}

		else if (choise == 2)
		{
			robot->disconnect();

			cout << endl;
		}

		else if (choise == 0)
		{
			break;
		}
	}
}

/**
* \brief hareketi kontrol eden fonksiyonlar�n menusu
*/

void MoveMenu(RobotControl robot)
{
	int choise;

	while (true)
	{
		cout << "1. Go Forward" << endl;
		cout << "2. Go Backward" << endl;
		cout << "3. Turn Left" << endl;
		cout << "4. Turn Right" << endl;
		cout << "5. Stop Turn" << endl;
		cout << "6. Stop Move" << endl;
		cout << "7. Where is the robot?" << endl;
		cout << "0. Back" << endl << endl;

		cin >> choise;

		if (choise == 1)
		{
			int speed;
			cout << "Enter speed : ";
			cin >> speed;
			robot.forward(speed);
		}

		else if (choise == 2)
		{
			int speed;
			cout << "Enter speed : ";
			cin >> speed;
			robot.backward(speed);
		}

		else if (choise == 3)
		{
			robot.turnLeft();
		}

		else if (choise == 4)
		{
			robot.turnRight();
		}

		else if (choise == 5)
		{
			robot.stopTurn();
		}

		else if (choise == 6)
		{
			robot.stopmove();
		}

		else if (choise == 7)
		{
			robot.print(sonars, laserData);
		}

		else if (choise == 0)
		{
			break;
		}
	}
}

