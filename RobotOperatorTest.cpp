#include<iostream>
#include"RobotOperator.h"

using namespace std;
int main()
{
	cout << "Create the new password :";
	int password;
	cin >> password;

	RobotOperator test(password);
	int code, selection;

	while (1)
	{
		cout << "Connect ->1, Print Information -> 2, Exit ->0 ";
		cin >> selection;

		if (selection == 1)
		{
			cout << "Please enter the password: ";
			cin >> code;
			if (test.checkAcessCode(code) == true)
			{
				cout << "Connected! " << endl;
			}
			else
				cout << "Password does not match!"<<endl;
		}
		else if (selection == 2)
		{
			test.print();
		}
		else if (selection == 0)
		{
			break;
		}
	}
	system("pause");
}