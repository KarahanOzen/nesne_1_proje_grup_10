#include"Record.h"

bool Record::openFile()
{
	 fstream file(fileName, std::fstream::in | std::fstream::out | std::fstream::app);
	 return   (file.is_open());
}
bool Record::closeFile()
{

	file.close();
	if (file.is_open())
	{
		return false;
	}
	return true;
}

string Record::readLine()
{
	fstream file(fileName, std::fstream::in| std::fstream::app);
	string line;

	getline(file, line);

	return line;
}

bool Record::writeLine(string line)
{
	fstream file(fileName, std::fstream::out | std::fstream::app);
	if (file.is_open())
	{
		file << line << endl;
		return true;
	}
	else
		return false;
}

void Record::setFileName(string name)
{
	fileName = name;
}

void Record::operator<<(string str)
{
	fstream file(fileName, std::fstream::out | std::fstream::app);
	file << str <<endl;
	
}

string Record::operator >> (ifstream &readFile)
{
	string line;			// S�n�f d���ndaki ba�ka bir dosyadan input almak i�in gerekli 

	if (readFile.is_open())
	{
		getline(readFile, line, (char)readFile.eof());
	
	}
	return line;
}

