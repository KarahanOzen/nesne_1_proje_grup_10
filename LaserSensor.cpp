#include "LaserSensor.h"

LaserSensor::LaserSensor(PioneerRobotAPI *robotAPI)
{
	robotAPI->getLaserRange(ranges);
}

LaserSensor::~LaserSensor()
{

}

float LaserSensor::getRange(int index)
{
	return ranges[index];
}

void LaserSensor::updateSensor(float ranges[])
{
	for (int i = 0; i < size_; i++) {
		this->ranges[i] = ranges[i];
	}
}

float LaserSensor::getMax(int &index)
{
	
	
	float max = ranges[index];
	for (index = 0; index < size_; index++) {
		if (ranges[index] > max) {
			max = ranges[index];
		}
	}
	index = 0;
	for (index = 0; index < size_; index++) {
		if (ranges[index] == max) {
			return index;
		}
	}
}

float LaserSensor::getMin(int &index)
{
	int index = 0;
	float min = ranges[index];
	for (index = 1; index < size_; index++) {
		if (ranges[index] < min) {
			min = ranges[index];
		}
	}
	
	for (index = 0; index < size_; index++) {
		if (ranges[index] == min) {
			return index;
		}
	}
	
}

float LaserSensor::operator[](int index)
{
	return this->getRange(index);
}

float LaserSensor::getAngle(int index)
{
	index = index - (index % 181) * 181;
	return index;
}

float LaserSensor::getClosestRange(float startAngle, float endAngle, float &angle)
{
	if (fabs(startAngle - angle) < fabs(endAngle - angle))
		return fabs(startAngle - angle);
	else
		return fabs(endAngle - angle);
}
