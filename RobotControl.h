#pragma once
#include<iostream>
#include"Pose.h"
#include"PioneerRobotAPI.h"

using namespace std;
/*
* \brief robotun kontrolunu ger�eklestiren s�n�f.
* \author Murat Caliskan.
*/

class RobotControl {
private:
	Pose* position;
	PioneerRobotAPI* robotAPI;
	int state;

public:
	/*
	* \brief Constructor 
	* \param pioneerrobotap� den bir pointer al�r.
	*/
	RobotControl(PioneerRobotAPI*);
	/*
	* \brief Destructor.
	*/
	~RobotControl();
	/*
	* \brief robotun sola donmesini saglayan fonksiyon.
	*/
	void turnLeft();
	/*
	* \brief robotun saga donmesini saglayan fonksiyon.
	*/
	void turnRight();
	/*
	* \brief robotun ileri gitmesini saglayan fonksiyon.
	* \param speed:fonksiyonun hangi h�zla gidecegini al�r.
	*/
	void forward(float speed);
	/*
	* \brief laser ve sonar rangelari yazdiran fonksiyon.
	* \param sonars:sonar� tutan dizi.
	* \param lasers: laseri tutan dizi.
	*/
	void print(float [],float[]);
	/*
	* \brief robotun geriye gitmesini saglayan fonksiyon.
	* \param speed : hangi hizla gittigini al�r.
	*/
	void backward(float speed);
	/*
	*  \brief Protected olan pose verisinin degerini okumak icin kullaniriz.
	*/
	Pose getPose();
	/*
	* \brief Pose u atayan fonksiyon.
	*/
	void setPose(Pose);
	/*
	* \brief d�nmesini durduran fonksiyon.
	*/
	void stopTurn();
	/*
	* \brief robotun hareketini durduran fonksiyon.
	*/
	void stopmove();
};