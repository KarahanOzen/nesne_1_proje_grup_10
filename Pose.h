#pragma once
/**
* \brief Pozisyon olu�turan s�n�f
*/
class Pose
{
public:
	/**
	* \brief Pose yap�c�s�
	*/
	Pose();
	/**
	* \brief Pose yap�c�s� overload
	*/
	Pose(float _x);
	/**
	* \brief Pose yap�c�s� overload
	*/
	Pose(float _x, float _y);
	/**
	* \brief Pose yap�c�s� overload
	*/
	Pose(float _x, float _y, float _th);
	/**
	* \brief Pose y�k�c� fonksiyon
	*/
	~Pose();
	/**
	* \brief x pozisyonu d�nd�r�r
	*/
	float getX() const;
	/**
	* \brief x pozisyonu atamas� yapar
	*/
	void setX(float);
	/**
	* \brief Y pozisyonu d�nd�r�r
	*/
	float getY() const;
	/**
	* \brief Y posizyonu atamas� yapar
	*/
	void setY(float);
	/**
	* \brief Th de�erini d�nd�r�r
	*/
	float getTH() const;
	/**
	* \brief a�� de�erini atar
	*/
	void setTH(float);
	/**
	* \brief e�itlik kontrol� yapan operat�r
	*/
	bool operator ==(const Pose& pos);
	/**
	* \brief pozisyona ekleme yapan operat�r
	*/
	Pose operator +(const Pose& pos);
	/**
	* \brief pozisyondan ��karma yapan operat�r
	*/
	Pose operator -(const Pose& pos);
	/**
	* \brief pozisyona ekleme yapan operat�r
	*/
	Pose& operator +=(const Pose& pos);
	/**
	* \brief pozisyondan ��karma yapan operat�r
	*/
	Pose& operator -=(const Pose& pos);
	/**
	* \brief pozisyon k���kl�k kontrol�
	*/
	bool operator <(const Pose& pos);
	/**
	* \brief pozisyon b�y�kl�k kontrol�
	*/
	bool operator >(const Pose& pos);
	/**
	* \brief pozisyon bilgisi d�nd�r�r
	*/
	Pose getPose() const;
	/**
	* \brief pozisyon atamas� yapar
	*/
	void setPose(float _x, float _y, float _th);
	/**
	* \brief pozisyonlar aras� uzakl�k hesaplar
	*/
	float findDistanceTo(Pose pos) const;
	/**
	* \brief pozisyonlar aras� a�� hesaplar
	*/
	float findAngleTo(Pose pos);

private:
	float x;
	float y;
	float th;
};