#include<iostream>
#include "Encryption.h"

using namespace std;

int Encryption::encrypt(int data)
{
	int A[4];
	int temp;

	A[0] = data / 1000;
	A[1] = (data % 1000) / 100;
	A[2] = (data % 100) / 10;
	A[3] = data % 10;

	for (int i = 0; i < 4; i++)
	{
		A[i] = (A[i] + 7) % 10;
	}
	temp = A[0];
	A[0] = A[2];
	A[2] = temp;
	temp = A[1];
	A[1] = A[3];
	A[3] = temp;

	int a = 0;

	a += A[3];
	a += (A[2] * 10);
	a += (A[1] * 100);
	a += (A[0] * 1000);

	return a;

	
}

int Encryption::decrypt(int data2)
{
	int B[4];
	int temp;

	B[0] = data2 / 1000;
	B[1] = (data2 % 1000) / 100;
	B[2] = (data2 % 100) / 10;
	B[3] = data2 % 10;

	for (int i = 0; i < 4; i++)
	{
		B[i] = ((B[i] + 10)-7) % 10;

	}
	temp = B[0];
	B[0] = B[2];
	B[2] = temp;
	temp = B[1];
	B[1] = B[3];
	B[3] = temp;

	int a = 0;

	a += B[3];
	a += (B[2] * 10);
	a += (B[1] * 100);
	a += (B[0] * 1000);

	return a;
}

