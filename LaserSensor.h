#pragma once
#include"PioneerRobotAPI.h"
#include<cmath>
#define size_ 181
/**
* \author Bora Kutay KESKINOGLU
* \brief Laser mesafe sensoru i�in veri tutan ve yoneten s�n�f.
*/
class LaserSensor{

private:
	/*
	* \brief Laser mesafe sensoru icin degerleri float tan�mlanmis bir dizide tutar.
	*/
	float ranges[size_];
	PioneerRobotAPI robotAPI;
	
public:
	/*
	* \brief Constructor
	*/
	LaserSensor(PioneerRobotAPI*);
	/*
	* \brief Destructor
	*/
	~LaserSensor();
	/*
	* \brief Verilen indeksin mesafe degerini dondurur.
	*/
	float getRange(int);
	/*
	* \brief Mesafe degerlerini parametrede verilen dizi ile gunceller.
	*/
	void updateSensor(float ranges[]);
	/*
	* \brief Mesafe degerleri arasindaki en kucuk sayiyi ve indeksi dondurur.
	*/
	float getMax(int &index);
	/*
	* \brief Mesafe degerleri arasindaki en buyuk sayiyi ve indeksi dondurur.
	*/
	float getMin(int &index);
	/*
	* \brief Verilen indeksin mesafe degerini dondurur.getRange() ile benzer islevi gorur.
	*/
	float operator[](int);
	/*
	* \brief Indeksi verilen sensorun acisini hesaplar ve dondurur.
	*/
	float getAngle(int index);
	/*
	* \brief startAngle ve endAngle a��lar� aras�nda kalan mesafelerden en k���k olan�n�n a��s�n� angle �zerinde, mesafeyi ise return ile d�nd�r�r.
	*/
	float getClosestRange(float, float, float&);




};